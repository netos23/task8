package ru.fbtw.utils.cmd;

/**
 * Сообщения об ошибках при обработке флагов
 */
class ExceptionMessages {
	 static final String WRONG_FORMAT = "Не верно заданы параметры запуска программы.\n" +
			"Для получения помощи используйте флаг --help";
	 static final String PAIR_FLAG = "Один флаг указан дважды";
}
