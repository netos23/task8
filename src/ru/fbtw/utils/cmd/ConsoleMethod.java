package ru.fbtw.utils.cmd;

@FunctionalInterface
public interface ConsoleMethod {
	void execute(String ... args) throws Exception;
}
