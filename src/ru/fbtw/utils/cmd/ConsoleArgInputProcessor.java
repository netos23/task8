package ru.fbtw.utils.cmd;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;

import static ru.fbtw.utils.cmd.ExceptionMessages.*;

public class ConsoleArgInputProcessor {

	private HashMap<String, String> args;
	private HashMap<String, ConsoleMethod> flagEvents;

	public ConsoleArgInputProcessor() {
		args = new HashMap<>();
		flagEvents = new HashMap<>();
	}

	/**
	 * @param flag - флаг
	 * @param event - колбек, который будет вызваен если фалаг будет задан
	 * @return - builder style метода, возвращает себя
	 */
	public ConsoleArgInputProcessor setFlagEvent(String flag, ConsoleMethod event){
		flagEvents.put(flag,event);
		return this;
	}

	/**
	 * @param args - параметры запуска
	 * @throws Exception - ошибки связанные с неверными параметрами
	 */
	public void execute(String[] args) throws Exception{
		this.args = parseData(args);

		for (Map.Entry<String,String> flag : this.args.entrySet()) {
			if(flagEvents.containsKey(flag.getKey())) {
				flagEvents.get(flag.getKey()).execute(flag.getValue());
			}else{
				throw new InputMismatchException(WRONG_FORMAT);
			}
		}
	}

	/**
	 * @param flag - флаг
	 * @return - возвращает правду при наличии нужного флага
	 */
	public boolean hasFlag(String flag){
		return args.containsKey(flag);
	}


	/**
	 * Метод парсит флаги и параметры
	 * @param args - параметры запуска
	 * @return - разделенные флаги и параметры
	 * @throws InputMismatchException
	 * @throws IndexOutOfBoundsException
	 */
	private static  HashMap<String, String> parseData(String[] args)
			throws InputMismatchException, IndexOutOfBoundsException {

		HashMap<String, String> result = new HashMap<>();
		String tmp = "";
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];

			if(arg.startsWith("--")){

				String flag = args[i];

				if(result.containsKey(flag)){
					throw new InputMismatchException(PAIR_FLAG);
				}

				result.put(flag,null);
			}else if (arg.startsWith("-") && i < args.length -1) {

				String flag = args[i];

				if(result.containsKey(flag)){
					throw new InputMismatchException(PAIR_FLAG);
				}

				result.put(flag,args[i+1]);
				i++;
			}else{
				throw new InputMismatchException(WRONG_FORMAT);
			}

		}
		return result;
	}
}
