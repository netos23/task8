package ru.fbtw.rect_region_parser.core;

import ru.fbtw.utils.IntVector2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Region - вспомогательный класс. Обьединение смежных точек
 * пример региона:
 * #....
 * .####
 * .####
 * фактическая площадь - 9
 * ожидаемая площадь - 5 * 3 = 15
 * <p>
 * пример прямоугольно региона:
 * ****
 * ****
 * фактическая площадь - 8
 * ожидаемая площадь - 2 * 4 = 8
 */
public class Region {

	/**
	 * Сециальный неизменяемы обьект Region для случая если решения не существует
	 */
	public static final Region NEVER = new Region() {
		@Override
		public String toString() {
			return "[-1, -1, -1, -1]";
		}

	};
	private ArrayList<IntVector2> points;
	private int actualArea;
	private int minX, maxX;
	private int minY, maxY;


	public Region() {
		points = new ArrayList<>();
		minX = -1;
		minY = -1;

	}

	/**
	 * @param point - точка которую добовляют к региону
	 * @return - builder style метода возвращает обьект на котором был вызван
	 */
	public Region add(IntVector2 point) {
		points.add(point);

		actualArea++;

		if (point.getX() > maxX) {
			maxX = point.getX();
		}
		if (point.getX() < minX || minX == -1) {
			minX = point.getX();
		}
		if (point.getY() > maxY) {
			maxY = point.getY();
		}
		if (point.getY() < minY || minY == -1) {
			minY = point.getY();
		}

		return this;
	}

	public boolean isRect() {
		return actualArea == getExpectedArea();
	}

	public int[] getBounds() {
		return new int[]{minY, minX, maxY - minY + 1, maxX - minX + 1};
	}

	public IntVector2 getOrigin() {
		return new IntVector2(minX, maxY);
	}

	public int getExpectedArea() {
		return (maxX - minX + 1) * (maxY - minY + 1);
	}

	public ArrayList<IntVector2> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<IntVector2> points) {
		this.points.clear();
		actualArea = 0;

		for (IntVector2 point : points) {
			add(point);
		}
	}

	public int getActualArea() {
		return actualArea;
	}

	@Override
	public String toString() {
		return Arrays.toString(getBounds());
	}
}
