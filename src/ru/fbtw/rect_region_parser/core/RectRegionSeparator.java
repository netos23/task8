package ru.fbtw.rect_region_parser.core;

import ru.fbtw.utils.IntVector2;

import java.util.ArrayList;
import java.util.LinkedList;

public class RectRegionSeparator {


	private ArrayList<Region> regions;
	private LinkedList<IntVector2> uncheckedPoints;

	private int w, h;
	private boolean[][] coordinatePlane;


	public Region separate(boolean[][] coordinatePlane) {
		this.coordinatePlane = coordinatePlane;
		w = coordinatePlane[0].length;
		h = coordinatePlane.length;
		uncheckedPoints = map();


		regions = separateRegions();

		return searchBiggestRect();
	}


	/**
	 * @return - множество целочисленных точек на координатной плоскости, в переделах входных данных
	 */
	private LinkedList<IntVector2> map() {
		LinkedList<IntVector2> points = new LinkedList<>();

		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				points.add(new IntVector2(j, i));
			}
		}

		return points;
	}


	/**
	 * Поиском в ширину ищет все Region в матрице
	 * <p>
	 * Рассматриваются смежные точки слева, справа, сверху, снизу и по-диагоналям
	 *
	 * @return - найденные {@see Region} в матрице
	 */
	private ArrayList<Region> separateRegions() {
		ArrayList<Region> result = new ArrayList<>();
		LinkedList<IntVector2> queue = new LinkedList<>();

		while (!uncheckedPoints.isEmpty()) {

			queue.add(uncheckedPoints.pollFirst());
			Region tmp = null;

			while (!queue.isEmpty()) {
				IntVector2 point = queue.pollFirst();

				if (coordinatePlane[point.getY()][point.getX()]) {

					if (tmp == null) {
						tmp = new Region();
					}

					tmp.add(point);

					if (point.getX() > 0) {
						addPointToQueue(queue, point.getX() - 1, point.getY());
					}
					if (point.getX() < w - 1) {
						addPointToQueue(queue, point.getX() + 1, point.getY());
					}
					if (point.getY() > 0) {
						addPointToQueue(queue, point.getX(), point.getY() - 1);
					}
					if (point.getY() < h - 1) {
						addPointToQueue(queue, point.getX(), point.getY() + 1);
					}
					if (point.getX() > 0 && point.getY() > 0) {
						addPointToQueue(queue, point.getX() - 1, point.getY() - 1);
					}
					if (point.getX() < w - 1 && point.getY() > 0) {
						addPointToQueue(queue, point.getX() + 1, point.getY() - 1);
					}
					if (point.getX() > 0 && point.getY() < h - 1) {
						addPointToQueue(queue, point.getX() - 1, point.getY() + 1);
					}
					if (point.getX() < w - 1 && point.getY() < h - 1) {
						addPointToQueue(queue, point.getX() + 1, point.getY() + 1);
					}

				}
			}

			if (tmp != null) {
				result.add(tmp);
			}
		}
		return result;
	}

	private void addPointToQueue(LinkedList<IntVector2> queue, int newX, int newY) {
		IntVector2 nextPoint = new IntVector2(newX, newY);
		if (uncheckedPoints.contains(nextPoint)) {
			queue.add(nextPoint);
			uncheckedPoints.remove(nextPoint);
		}
	}


	/**
	 * По заданным условия ищет по найденным Region прямоугольники
	 *
	 * @return - Region с наибольшей площадью при том, что он самый верхний, левый
	 */
	private Region searchBiggestRect() {
		int maxIndex = -1;

		for (int i = 0; i < regions.size(); i++) {
			Region region = regions.get(i);

			if (region.isRect()) {
				if (maxIndex == -1) {
					maxIndex = i;
				} else {
					Region max = regions.get(maxIndex);

					if (region.getActualArea() > max.getActualArea()) {
						maxIndex = i;
					}

					if (region.getActualArea() == max.getActualArea()) {
						IntVector2 regionOrigin = region.getOrigin();
						IntVector2 maxOrigin = max.getOrigin();

						if (regionOrigin.getY() < maxOrigin.getY()) {
							maxIndex = i;
						}
						if (regionOrigin.getY() == maxOrigin.getY() && regionOrigin.getX() < maxOrigin.getX()) {
							maxIndex = i;
						}
					}
				}
			}
		}

		return maxIndex != -1
				? regions.get(maxIndex)
				: null;
	}


	public ArrayList<Region> getRegions() {
		return regions;
	}
}
