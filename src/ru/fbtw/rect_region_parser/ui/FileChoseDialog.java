package ru.fbtw.rect_region_parser.ui;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import org.omg.CORBA.PRIVATE_MEMBER;
import ru.fbtw.rect_region_parser.io.ExceptionMessages;

import java.io.File;

public class FileChoseDialog  extends Dialog{
	private final OnSumbitListner<File> onSubmitListener;
	private  File target;
	private  FileChooser fileChooser;
	private TextField pathInput;
	private FileType fileType;


	FileChoseDialog(String title, OnSumbitListner<File> onSubmitListener, String initVal, FileType ft) {
		this.onSubmitListener = onSubmitListener;
		this.fileType = ft;

		stage.initStyle(StageStyle.UTILITY);
		stage.setTitle(title);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setResizable(false);


		Label dirLabel = new Label("Path");

		pathInput = new TextField(initVal);
		pathInput.setMinWidth(300);
		HBox.setHgrow(pathInput, Priority.ALWAYS);

		fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(filter);

		Button choseBtn = new Button("...");

		choseBtn.setOnAction(this::pickFile);

		Button submit = new Button("OK");
		submit.setOnAction(this::submit);

		HBox content = new HBox(10,dirLabel,pathInput,choseBtn);
		VBox mainLayout = new VBox(20,content,submit);
		mainLayout.setAlignment(Pos.CENTER);
		mainLayout.setPadding(new Insets(20));
		Scene scene = new Scene(mainLayout);
		stage.setScene(scene);
	}

	private void submit(ActionEvent event) {
		try {
			File file = new File(pathInput.getText());
			onSubmitListener.onSumbit(file);
			stage.close();
		}catch (Exception ex){
			stage.close();
			DialogViewer.showError("Ошибка", ExceptionMessages.FILE_ERR);
		}

	}

	private  void pickFile(ActionEvent event){
		if(fileType == FileType.LOAD) {
			target = fileChooser.showOpenDialog(stage);
		}else{
			target = fileChooser.showSaveDialog(stage);
		}
		if(target!= null){
			pathInput.setText(target.getPath());
		}

	}

	private static final FileChooser.ExtensionFilter filter =
			new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");

}
