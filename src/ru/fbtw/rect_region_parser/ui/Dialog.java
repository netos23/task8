package ru.fbtw.rect_region_parser.ui;

import javafx.stage.Stage;

public abstract class Dialog {
	Stage stage;

	public Dialog() {
		stage = new Stage();
	}

	public void execute() {
		stage.show();
	}
}
