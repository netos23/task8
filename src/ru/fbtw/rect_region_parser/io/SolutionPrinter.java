package ru.fbtw.rect_region_parser.io;

import ru.fbtw.rect_region_parser.core.Region;

import java.io.PrintStream;

public class SolutionPrinter {
	private static final String INPUT_INFO = "Для данных:\n";
	private static final String RESULT_INFO = "\nРезультат:\n";
	private PrintStream out;
	private boolean isConsole;


	public SolutionPrinter(PrintStream output) {
		this(output, System.out == output);
	}

	public SolutionPrinter(PrintStream output, boolean isConsole) {
		this.out = output;
		this.isConsole = isConsole;
	}


	/**
	 * @param solution - входная матрица
	 * @param region   - решение
	 */
	public void printSolution(boolean[][] solution, Region region) {
		out.println(INPUT_INFO);
		printMatrix(solution);
		out.println(RESULT_INFO);
		out.println(region.toString());
	}

	/**
	 * @param solution - входная матрица
	 */
	private void printMatrix(boolean[][] solution) {
		for (int i = 0; i < solution.length; i++) {
			for (int j = 0; j < solution[0].length; j++) {
				out.printf("%s ", solution[i][j]);
			}
			out.println();
		}
	}


	/**
	 * Устанавливает поток вывода
	 *
	 * @param out - новый поток вывода
	 */
	public void setOutputSource(PrintStream out) {
		this.out = out;
	}
}
