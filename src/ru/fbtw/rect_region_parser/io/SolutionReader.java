package ru.fbtw.rect_region_parser.io;

import ru.fbtw.utils.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class SolutionReader {
	private static final String err = "Возникла ошибка попробуйте еще раз";
	private Scanner scanner;
	private boolean isConsole;

	public SolutionReader(Scanner input, boolean isConsole) {
		this.scanner = input;
		this.isConsole = isConsole;
	}

	/**
	 * Метод взависимости от входного потока считывает входную матрицу
	 *
	 * @return - входная матрица
	 */
	public boolean[][] readInput() {
		return isConsole ? readFromConsole() : readFromFile();
	}

	private boolean[][] readFromConsole() {
		int w, h;
		w = readInt("Ввидите ширину(целое число):");
		h = readInt("Ввидите высоту(целое число):");

		final String msg = String.format("Введите матрицу %d x %d логических значений\n" +
				"для значений  исспользуйте true(правда) / false(ложь):", w, h);
		System.out.println(msg);

		while (true) {
			try {
				return readMatrixBySize(h, w);
			} catch (Exception ex) {
				System.out.println(err);
			}
		}
	}

	private int readInt(String msg) {
		int val;
		while (true) {
			System.out.println(msg);
			try {
				val = scanner.nextInt();
				break;
			} catch (Exception ex) {
				System.out.println(err);
			}
		}
		return val;
	}

	/**
	 * Сначала определяет тип входнго файла: c заданными размерами и без.
	 * Считываат данные из файла
	 *
	 * @return - входная матрица
	 */
	private boolean[][] readFromFile() {
		String first = scanner.nextLine();
		boolean[][] res;
		// попытка прочитать размер массива
		try {
			int[] bounds = Arrays.stream(first.split(" "))
					.mapToInt(Integer::parseInt)
					.toArray();

			// если в первой строке два числа длина и ширина
			// тогда по заданным границам считать массив
			if (bounds.length == 2) {
				int row = bounds[0];
				int column = bounds[1];

				res = readMatrixBySize(row, column);
			} else {
				throw new InputMismatchException(ExceptionMessages.INPUT_ERR);
			}
		} catch (NumberFormatException ex) {
			// Если длина и ширина не заданы
			ArrayList<String> stringStore = new ArrayList<>();
			stringStore.add(first);

			while (scanner.hasNext()) {
				stringStore.add(scanner.nextLine());
			}

			res = mapToBool(stringStore);

			if (!ArrayUtils.isRectMatrix(res)) {
				throw new InputMismatchException(ExceptionMessages.INPUT_ERR);
			}
		} catch (IndexOutOfBoundsException ex) {
			throw new IndexOutOfBoundsException(ExceptionMessages.INPUT_ERR);
		} catch (InputMismatchException ex){
			throw new InputMismatchException(ExceptionMessages.INPUT_ERR);
		}

		return res;
	}

	/**
	 * @param row    - количество рядов
	 * @param column - количество колонок
	 * @return - входная матрица
	 */
	private boolean[][] readMatrixBySize(int row, int column) {
		boolean[][] res;
		res = new boolean[row][column];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				res[i][j] = scanner.nextBoolean();
			}
		}
		return res;
	}


	public boolean[][] mapToBool(ArrayList<String> booleans) {
		boolean[][] temp = new boolean[booleans.size()][];

		for (int i = 0; i < booleans.size(); i++) {
			String s = booleans.get(i);

			temp[i] = ArrayUtils.toPrimitive(Arrays.stream(s.split(" "))
					.map(Boolean::parseBoolean)
					.toArray(Boolean[]::new)
			);
		}
		return temp;
	}

	public Scanner getScanner() {
		return scanner;
	}

	public void setInputSource(Scanner scanner, boolean isConsole) {
		this.scanner = scanner;
		this.isConsole = isConsole;
	}
}
