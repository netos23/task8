package ru.fbtw.rect_region_parser.io;

/**
 * Сообщения об ошибках при вводе - выводе
 */
public class ExceptionMessages {
	public static final String FILE_ERR = "Указан не верный путь или фаил не удалось открыть";
	public static final String FILE_TYPE_ERR = "Указан не верный путь или фаил не удалось открыть";
	public static final String INPUT_ERR = "Данные заданы в не верном формате";
	public static final String UNKNOWN = "Неизвестная ошибка";
}
