package ru.fbtw.rect_region_parser;


import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import ru.fbtw.rect_region_parser.core.RectRegionSeparator;
import ru.fbtw.rect_region_parser.core.Region;
import ru.fbtw.rect_region_parser.io.ExceptionMessages;
import ru.fbtw.rect_region_parser.io.SolutionPrinter;
import ru.fbtw.rect_region_parser.io.SolutionReader;
import ru.fbtw.rect_region_parser.ui.DialogViewer;
import ru.fbtw.rect_region_parser.ui.FileType;
import ru.fbtw.utils.ArrayUtils;
import ru.fbtw.utils.IntVector2;
import ru.fbtw.utils.cmd.ConsoleArgInputProcessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Main extends Application {
	private static final double CELL_WIDTH = 50;
	private static final double CELL_HEIGHT = 50;
	private static final Color UNSELECTED_COLOR = Color.GRAY;
	private static final Color SELECTED_COLOR = Color.BLACK;
	private static final Color RESULT_COLOR = Color.RED;
	private static final int MIN_WIDTH = 650;
	private static final int MIN_HEIGHT = 400;

	/**
	 * Класс содержащий реализацию логики работы програмы
	 */
	private static RectRegionSeparator regionSeparator;

	private static PrintStream output = System.out;
	private static Scanner input = new Scanner(System.in);
	private static boolean isHeadless = false;

	private static boolean isConsoleInput = true, isConsoleOutput = true;

	private static SolutionPrinter printer;
	private static SolutionReader reader;
	private static boolean[][] inputData;

	private static int initWidth = 1;
	private static int initHeight = 1;

	private TextField widthInput;
	private TextField heightInput;
	private Pane body;
	private Rectangle[][] cellsStorage;
	private Label textResultLabel;
	private Scene scene;
	private double localCellWidth;
	private ScrollPane bodyContainer;
	private double localCellHeight;
	private boolean isShowResult;


	/**
	 * Метод переопределяет ввод с консоли на ввод с файла
	 *
	 * @param args - в параметре 0 содержиться путь к файлу который следует загрузить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setInput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];
			isConsoleInput = false;

			if (validateFileName(filename)) {

				File inputFile = new File(filename);
				input = new Scanner(inputFile);
			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}

	}

	/**
	 * @param fileName - имя файло которое следует проверить
	 * @return - возвращает true если фаил является текстовым
	 */
	private static boolean validateFileName(String fileName) {
		return fileName.endsWith(".txt");
	}

	/**
	 * Метод переопределяет вывод в консоли на вывод в файл
	 *
	 * @param args - в параметре 0 содержиться путь к файлу в который следует сохранить
	 * @throws FileNotFoundException
	 * @throws InputMismatchException
	 */
	private static void setOutput(String... args) throws FileNotFoundException, InputMismatchException {
		try {
			String filename = args[0];
			isConsoleOutput = false;

			if (validateFileName(filename)) {
				File outputFile = new File(filename);
				output = new PrintStream(outputFile);

			} else {
				throw new InputMismatchException(ExceptionMessages.FILE_TYPE_ERR);
			}

		} catch (FileNotFoundException ex) {
			throw new FileNotFoundException(ExceptionMessages.FILE_ERR);
		}
	}

	/**
	 * Отключает оконный интерфейс
	 *
	 * @param args - аргументы отсутствуют
	 */
	private static void setIsHeadless(String... args) {
		isHeadless = true;
	}

	/**
	 * Просмотр помощи
	 *
	 * @param args - аргументы отсутствуют
	 */
	private static void getHelp(String... args) {
		String message = "Добро пожаловать в программу поиска прямоугольников!\n" +
				"Программа позволяет искать прямоугольники)\n" +
				"список команд: \n" +
				"--headless  позволяет отключить оконный интерфейс\n" +
				"--help запросить помощь\n" +
				"-i [dir] указать деррикторию из которой следует загрузить данные,\n" +
				"\tесли в headless режиме флаг не указан, то ввод необходимо произвести с консоли\n" +
				"-o [dir] указать деррикторию в которую следует сохранить данные,\n" +
				"\tесли в headless режиме флаг не указан, то вывод будет произведен в консоль\n";
		System.out.println(message);
		System.exit(0);
	}

	/**
	 * Инициализация программы
	 *
	 * @param args - параметры запуска программы из консоли
	 */
	public static void main(String[] args) {
		regionSeparator = new RectRegionSeparator();

		ConsoleArgInputProcessor processor = new ConsoleArgInputProcessor();

		processor.setFlagEvent("--help", Main::getHelp)
				.setFlagEvent("--headless", Main::setIsHeadless)
				.setFlagEvent("-i", Main::setInput)
				.setFlagEvent("-o", Main::setOutput);

		try {
			processor.execute(args);

			printer = new SolutionPrinter(output, isConsoleOutput);
			reader = new SolutionReader(input, isConsoleInput);

			if (isHeadless) {
				showWelcomeMessage();

				inputData = reader.readInput();
				Region result = regionSeparator.separate(inputData);

				if (result == null) {
					result = Region.NEVER;
				}

				printer.printSolution(inputData, result);

				System.exit(0);
			} else {

				if (processor.hasFlag("-i")) {
					read();
				}
				launch(args);

			}
		} catch (Exception ex) {
			//ex.printStackTrace();
			String message = ex.getMessage();
			if (message == null) message = ExceptionMessages.UNKNOWN;
			System.err.println(message);
			System.exit(-1);
		}
	}

	/**
	 * При использовании оконного интерфейса метод загружает {@code inputData}
	 */
	private static void read() {
		inputData = reader.readInput();
		initWidth = inputData[0].length;
		initHeight = inputData.length;
	}


	/**
	 * При исспользовании консоли как средства ввода, выводит приветсвенное сообщение
	 */
	private static void showWelcomeMessage() {
		String message = "Добро пожаловать в программу поиска прямоугольников\n";

		if (isConsoleInput) {
			output.println(message);
		}
	}

	/**
	 * Задание разметки и контента для сцены {@param primaryStage}
	 */
	@Override
	public void start(Stage primaryStage) {
		isShowResult = false;
		primaryStage.setTitle("Поиск прямоугольников");

		BorderPane mainLayout = new BorderPane();

		GridPane header = buildHeader();

		bodyContainer = new ScrollPane();
		bodyContainer.setFitToHeight(false);
		bodyContainer.setFitToWidth(false);

		body = new Pane();


		updateBody(null);
		bodyContainer.setContent(body);

		GridPane footer = buildFooter();

		mainLayout.setTop(header);
		mainLayout.setCenter(bodyContainer);
		mainLayout.setBottom(footer);

		primaryStage.setMinWidth(MIN_WIDTH);
		primaryStage.setMinHeight(MIN_HEIGHT);

		scene = new Scene(mainLayout, MIN_WIDTH, MIN_HEIGHT);

		scene.widthProperty()
				.addListener(this::resize);
		scene.heightProperty()
				.addListener(this::resize);


		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Метод задает стили, колбеки и разметку нижней части интерфейса
	 *
	 * @return - разметку нижней части интерфейса
	 */
	private GridPane buildFooter() {
		textResultLabel = new Label("Результат: ");
		textResultLabel.setVisible(false);

		Button pushBtn = new Button("Найти");
		pushBtn.setOnAction(this::find);

		Button saveBtn = new Button("Сохранить");
		saveBtn.setOnAction(this::saveOnClick);

		Button loadBtn = new Button("Загрузить");
		loadBtn.setOnAction(this::loadOnClick);

		Button clearBtn = new Button("Отчистить");
		clearBtn.setOnAction(this::clear);

		GridPane footerLayout = new GridPane();


		footerLayout.setPadding(new Insets(10.0));
		footerLayout.setVgap(10.0);
		footerLayout.setHgap(30.0);
		footerLayout.setAlignment(Pos.CENTER);

		footerLayout.add(textResultLabel, 0, 0, 4, 1);
		footerLayout.add(pushBtn, 0, 1);
		footerLayout.add(saveBtn, 1, 1);
		footerLayout.add(loadBtn, 2, 1);
		footerLayout.add(clearBtn, 3, 1);


		return footerLayout;
	}

	/**
	 * Этот метод отчищает холст
	 */
	private void clear(ActionEvent event) {
		inputData = null;
		updateBody(null);
	}

	/**
	 * Метод задает стили, колбеки и разметку верхней части интерфейса
	 *
	 * @return - верхней части интерфейса
	 */
	private GridPane buildHeader() {
		Label message = new Label("Введите границы поля");
		message.setAlignment(Pos.CENTER);
		Label widthLabel = new Label("Ширина");
		Label heightLabel = new Label("Высота");

		widthInput = new TextField(Integer.toString(initWidth));
		widthInput.setOnAction(this::updateBody);
		heightInput = new TextField(Integer.toString(initHeight));
		heightInput.setOnAction(this::updateBody);

		GridPane headerLayout = new GridPane();
		headerLayout.setAlignment(Pos.CENTER);
		headerLayout.setPadding(new Insets(10.0));
		headerLayout.setVgap(20);
		headerLayout.setHgap(30);


		headerLayout.add(message, 2, 0, 2, 1);
		headerLayout.add(widthLabel, 0, 1);
		headerLayout.add(widthInput, 2, 1);
		headerLayout.add(heightLabel, 3, 1);
		headerLayout.add(heightInput, 4, 1);

		return headerLayout;
	}

	/**
	 * Этот метод обновляет контент центра {@link #body}, а при запуске программы инициализирует его.
	 * <p>
	 * Для начала проверяется возможно ли преобразовать значение длины и ширины в число.
	 * Если это не возможно, то присваиваются значения по умолчанию
	 * <p>
	 * Метод вызывается при вызове {@link #resize(ObservableValue, Number, Number)}, инициализации
	 * или при вводе данных.
	 */
	private void updateBody(ActionEvent event) {
		int cellWidthCount, cellHeightCount;

		try {
			cellWidthCount = Integer.parseInt(widthInput.getText());
		} catch (Exception ex) {
			cellWidthCount = 1;
			widthInput.setText("1");
		}

		try {
			cellHeightCount = Integer.parseInt(heightInput.getText());
		} catch (Exception ex) {
			cellHeightCount = 1;
			heightInput.setText("1");
		}

		body.getChildren().clear();
		cellsStorage = new Rectangle[cellHeightCount][cellWidthCount];

		resizeInputData(cellWidthCount, cellHeightCount);

		updateCellSize(cellWidthCount, cellHeightCount);

		for (int i = 0; i < cellHeightCount; i++) {
			for (int j = 0; j < cellWidthCount; j++) {
				Rectangle tmp = getNewCell(i, j);

				cellsStorage[i][j] = tmp;
				updateCellColor(tmp);

				body.getChildren().add(tmp);
			}
		}


	}

	/**
	 * Метод копирует значения из {@code inputData} во временное хранилище, изменяет ее размер
	 * и возвращает данные  в переделах новых границ
	 *
	 * @param cellWidthCount  - количество колонок на холсте
	 * @param cellHeightCount - количество рядов на холсте
	 */
	private void resizeInputData(int cellWidthCount, int cellHeightCount) {
		if (inputData != null) {
			// Сохраняем значение полей в пределах новых границ
			boolean[][] nextInputData = new boolean[cellHeightCount][cellWidthCount];
			ArrayUtils.matrixCopy(inputData, nextInputData);

			//Копируем их в inputData с новыми границами
			inputData = nextInputData;
		} else {
			inputData = new boolean[cellHeightCount][cellWidthCount];
		}
	}


	/**
	 * Сздает ячейку
	 *
	 * @param i - колонка ячейки
	 * @param j - ряд ячейки
	 * @return - готовая ячейка
	 */
	private Rectangle getNewCell(int i, int j) {
		Rectangle tmp = new Rectangle(localCellWidth * j, localCellHeight * i,
				localCellWidth, localCellHeight);
		tmp.setStroke(Color.WHITE);
		tmp.setOnMouseClicked(this::onCellClick);
		return tmp;
	}

	/**
	 * @param cellWidthCount  - количество колонок в холсте
	 * @param cellHeightCount - количество рядов в холсте
	 */
	private void updateCellSize(int cellWidthCount, int cellHeightCount) {
		double sceneWidth = scene != null
				? scene.getWidth()
				: MIN_WIDTH;

		double sceneHeight = bodyContainer != null && bodyContainer.getHeight() > 1
				? bodyContainer.getHeight()
				: MIN_HEIGHT;

		localCellWidth = CELL_WIDTH * cellWidthCount < sceneWidth
				? sceneWidth / cellWidthCount
				: CELL_WIDTH;

		localCellHeight = CELL_HEIGHT * cellHeightCount < sceneHeight
				? sceneHeight / cellWidthCount
				: CELL_HEIGHT;
	}

	/**
	 * Метод обратного вызова при клике на ячейку
	 * Изменяет ее значение на противоположное и обновляет цвет
	 *
	 * @param event - event клика на ячейку
	 */
	private void onCellClick(MouseEvent event) {
		Rectangle cell = (Rectangle) event.getSource();

		int w = (int) Math.round(cell.getX() / localCellWidth);
		int h = (int) Math.round(cell.getY() / localCellHeight);
		inputData[h][w] = !inputData[h][w];
		updateCellColor(cell);
		clearResult();

	}

	/**
	 * Обновляет цвет целевой ячейки
	 *
	 * @param cell - целевая ячейка
	 */
	private void updateCellColor(Rectangle cell) {
		int w = (int) Math.round(cell.getX() / localCellWidth);
		int h = (int) Math.round(cell.getY() / localCellHeight);
		cell.setFill((inputData[h][w]) ? SELECTED_COLOR : UNSELECTED_COLOR);
	}

	/**
	 * Метод обратного вызова при изменении размера окна
	 *
	 * @param observableValue - параметр метода функционального интерфейса {@see ChangeListener<T>}
	 * @param oldSceneWidth   - параметр метода функционального интерфейса {@see ChangeListener<T>}
	 * @param newSceneWidth   - параметр метода функционального интерфейса {@see ChangeListener<T>}
	 */
	private void resize(
			ObservableValue<? extends Number> observableValue,
			Number oldSceneWidth,
			Number newSceneWidth
	) {
		updateBody(null);
	}

	/**
	 * Метод обратного вызова при нажатии на кнопку загрузки файла
	 *
	 * @param event - параметр метода функционального интерфейса
	 */
	private void loadOnClick(ActionEvent event) {
		DialogViewer.showFilePickDialog("Загрузить", this::load_file, "", FileType.LOAD);
	}


	/**
	 * Загружает фаил и обрабатывает ошибки при его открытии
	 *
	 * @param file - фаил который был выбран пользователем в диалоговом окне
	 */
	private void load_file(File file) {
		if (file != null) {
			try {
				Scanner in = new Scanner(file);
				reader.setInputSource(in, false);
				read();

				widthInput.setText(Integer.toString(initWidth));
				heightInput.setText(Integer.toString(initHeight));

				updateBody(null);
			} catch (FileNotFoundException e) {
				System.err.println(ExceptionMessages.FILE_ERR);
				DialogViewer.showError("Ошибка", ExceptionMessages.FILE_ERR);
			} catch (Exception ex) {
				String msg = ex.getMessage();
				if (msg == null) msg = ExceptionMessages.UNKNOWN;
				System.err.println(msg);
				DialogViewer.showError("Ошибка", msg);
			}
		}
	}

	/**
	 * Метод обратного вызова при нажатии на кнопку сохранения файла
	 *
	 * @param event - параметр метода функционального интерфейса
	 */
	private void saveOnClick(ActionEvent event) {
		DialogViewer.showFilePickDialog("Сохранить", this::save_file, "", FileType.SAVE);
	}

	private void save_file(File file) {
		try {
			PrintStream out = new PrintStream(file);
			printer.setOutputSource(out);

			Region region = find(null);
			printer.printSolution(inputData, region);
		} catch (FileNotFoundException e) {
			System.err.println(ExceptionMessages.FILE_ERR);
			DialogViewer.showError("Ошибка", ExceptionMessages.FILE_ERR);
		} catch (Exception ex) {
			System.err.println(ExceptionMessages.UNKNOWN);
			DialogViewer.showError("Ошибка", ExceptionMessages.UNKNOWN);
		}
	}

	/**
	 * Запускает поиск прямоугольников
	 *
	 * @param event - параметр метода функционального интерфейса
	 * @return - результат работы программы
	 */
	private Region find(ActionEvent event) {
		Region result = regionSeparator.separate(inputData);

		if (result == null) {
			result = Region.NEVER;
		} else {
			showResult(result);
		}
		textResultLabel.setVisible(true);
		String resultText = String.format("Результат: %s", result);
		textResultLabel.setText(resultText);
		return result;
	}

	/**
	 * Скрывает временно найденный результат
	 */
	private void clearResult() {
		if (isShowResult) {
			textResultLabel.setVisible(false);
			updateBody(null);
			isShowResult = false;
		}
	}

	/**
	 * Временно показывает результат
	 *
	 * @param region - результат работы, целевой прямоугольник
	 */
	private void showResult(Region region) {
		isShowResult = true;
		for (IntVector2 point : region.getPoints()) {
			cellsStorage[point.getY()][point.getX()].setFill(RESULT_COLOR);
		}
	}
}
